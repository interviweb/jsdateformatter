# JSDateFormatter

Easy library for date formatting.

**Install**: npm i jsdateformatter

## API

* **JSDateFormatter.parse(format, date, utc)** - get string date formatted
* **JSDateFormatter.getMap(format, date, utc)** - get parsed date, return object *{YYYY: 2018, ...}*

### Arguments

* **format** - string, see below
* **date** - Date object or null
* **utc** - boolean, use UTC time if true

## Format

YYYY-MM-DD d hh:mm:ss.SSS, A, B

* **Y** - year
* **M** - month
* **D** - month day
* **d** - week day
* **h** - 24-h hours
* **H** - AM/PM 12-h hours
* **m** - minutes
* **s** - seconds
* **S** - milliseconds
* **A** - full localized month day
* **a** - short (first 3 symbols) localized month day
* **B** - full localized week day
* **b** - short (first 3 symbols) localized week day
* **p** - AM/PM

**Y** - cutting on the left

**S** - cutting on the right

**M**, **D**, **h**, **m**, **s** - no add first zero

**MM**, **DD**, **hh**, **mm**, **ss** - only two numbers, add first zero if 0-9

### Examples

* hh:mm **->** 00:01
* YYYY-MM-DD d hh:mm:ss.SSS **->** 2018-06-26 4 00:01:24.299
* YY-M-D d h:m:s.SS **->** 18-6-26 4 0:1:24.29
* a, DD b, H:mm p **->** чет, 26 июл, 6:37 PM
