const JSDateFormatter = (function() {
    let _date = new Date();
    let defaultFormat = 'hh:mm';

    function getYear() {
        let year = String(_date.getFullYear());
        return {
            'Y': year.slice(3),
            'YY': year.slice(2),
            'YYY': year.slice(1),
            'YYYY': year
        }
    }

    function getMonth() {
        let m = _date.getMonth();
        let month = String(m);
        if (month.length === 1) month = '0' + month;
        return {
            'M': String(m),
            'MM': month
        }
    }

    function getMonthDay() {
        let d = _date.getDate();
        let monthDay = String(d);
        if (monthDay.length === 1) monthDay = '0' + monthDay;
        return {
            'D': String(d),
            'DD': monthDay
        }
    }

    function getWeekDay() {
        return {
            'd': String(_date.getDay())
        }
    }

    function getHours() {
        let h = _date.getHours();
        //AM/PM time
        let mph = h === 0 ? 12 : (h > 12 ? h - 12 : h);
        let smph = String(mph);
        if (smph.length === 1) smph = '0' + smph;
        //24-h time
        let hours = String(h);
        if (hours.length === 1) hours = '0' + hours;
        return {
            'H': String(mph),
            'HH': smph,
            'h': String(h),
            'hh': hours
        }
    }

    function getMinutes() {
        let m = _date.getMinutes();
        let min = String(m);
        if (min.length === 1) min = '0' + min;
        return {
            'm': String(m),
            'mm': min
        }
    }

    function getSeconds() {
        let s = _date.getSeconds();
        let sec = String(s);
        if (sec.length === 1) sec = '0' + sec;
        return {
            's': String(s),
            'ss': sec
        }
    }

    function getMilliseconds() {
        let ms = String(_date.getMilliseconds());
        while (ms.length < 3) ms = '0' + ms;
        return {
            'S': ms.slice(2),
            'SS': ms.slice(1),
            'SSS': ms
        }
    }

    function getLocaleDayMonth() {
        let a = _date.toLocaleDateString(window.navigator.language, {'weekday': 'long'});
        return {
            'a': a.slice(0, 3),
            'A': a
        }
    }

    function getLocaleMonth() {
        let b = _date.toLocaleDateString(window.navigator.language, {'month': 'long'});
        return {
            'b': b.slice(0, 3),
            'B': b
        }
    }

    function getAmPm() {
        return {
            'p': _date.getHours() <= 12 ? 'AM' : 'PM'
        }
    }

    function getFullMap() {
        return Object.assign(
            getYear(), getMonth(), getMonthDay(), getWeekDay(), getHours(),
            getMinutes(), getSeconds(), getMilliseconds(),
            getLocaleDayMonth(), getLocaleMonth(), getAmPm()
        );
    }

    function parseString(str) {
        if (str.search('Y') != -1) {
            let y = getYear();
            str = str.replace(new RegExp('YYYY', 'g'), y['YYYY']);
            str = str.replace(new RegExp('YYY', 'g'), y['YYY']);
            str = str.replace(new RegExp('YY', 'g'), y['YY']);
            str = str.replace(new RegExp('Y', 'g'), y['Y']);
        }
        if (str.search('M') != -1) {
            let m = getMonth();
            str = str.replace(new RegExp('MM', 'g'), m['MM']);
            str = str.replace(new RegExp('M', 'g'), m['M']);
        }
        if (str.search('d') != -1) {
            str = str.replace(new RegExp('d', 'g'),  getWeekDay()['d']);
        }
        if (str.search('D') != -1) {
            let d = getMonthDay();
            str = str.replace(new RegExp('DD', 'g'), d['DD']);
            str = str.replace(new RegExp('D', 'g'), d['D']);
        }
        if (str.search('h') != -1 || str.search('H') != -1) {
            let h = getHours();
            str = str.replace(new RegExp('hh', 'g'), h['hh']);
            str = str.replace(new RegExp('h', 'g'), h['h']);
            str = str.replace(new RegExp('HH', 'g'), h['HH']);
            str = str.replace(new RegExp('H', 'g'), h['H']);
        }
        if (str.search('m') != -1) {
            let m = getMinutes();
            str = str.replace(new RegExp('mm', 'g'), m['mm']);
            str = str.replace(new RegExp('m', 'g'), m['m']);
        }
        if (str.search('s') != -1) {
            let s = getSeconds();
            str = str.replace(new RegExp('ss', 'g'), s['ss']);
            str = str.replace(new RegExp('s', 'g'), s['s']);
        }
        if (str.search('S') != -1) {
            let s = getMilliseconds();
            str = str.replace(new RegExp('SS', 'g'), s['SS']);
            str = str.replace(new RegExp('S', 'g'), s['S']);
        }
        if (str.search('a') != -1 || str.search('A') != -1) {
            let a = getLocaleDayMonth();
            str = str.replace(new RegExp('a', 'g'), a['a']);
            str = str.replace(new RegExp('A', 'g'), a['A']);
        }
        if (str.search('b') != -1 || str.search('B') != -1) {
            let b = getLocaleMonth();
            str = str.replace(new RegExp('b', 'g'), b['b']);
            str = str.replace(new RegExp('B', 'g'), b['B']);
        }
        if (str.search('p') != -1) {
            let p = getAmPm();
            str = str.replace(new RegExp('p', 'g'), p['p']);
        }
        return str;
    }

    function init(format, date, utc) {
        if (date) _date = date;
        if (utc) {
            _date = new Date(
                _date.getUTCFullYear(), _date.getUTCMonth(), _date.getUTCDate(),
                _date.getUTCHours(), _date.getUTCMinutes(), _date.getUTCSeconds(),
                _date.getUTCMilliseconds()
            );
        }
    }

    return {
        'parse': function(format, date, utc) {
            init(format, date, utc);
            return format ? parseString(format) : parseString(defaultFormat);
        },
        'getMap': function (format, date, utc) {
            init(format, date, utc);
            return getFullMap();
        }
    }
}());
if (module && module.exports) {
    module.exports = JSDateFormatter;
}
